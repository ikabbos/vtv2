package com.example.vt2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.rengwuxian.materialedittext.MaterialEditText;

public class ForgettingPassActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private ProgressBar progressBar;
    private TextView resetState;
    private MaterialEditText emailAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetting_pass);
        Button sendMsgBtn = findViewById(R.id.sendMsgBtn);
        Button returnBtn = findViewById(R.id.returnBtn);
        firebaseAuth = FirebaseAuth.getInstance();
        emailAddress = findViewById(R.id.email);
        resetState = findViewById(R.id.resetText);
        ProgressBar progressBar = findViewById(R.id.progressbar);


        sendMsgBtn.setOnClickListener(this::onClick);
        returnBtn.setOnClickListener(this::onClick);


    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendMsgBtn:
                progressBar.setVisibility(View.VISIBLE);
                firebaseAuth.fetchSignInMethodsForEmail(emailAddress.getText().toString()).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                    @Override
                    public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                        if(task.getResult().getSignInMethods().isEmpty())
                        {
                            progressBar.setVisibility(View.GONE);
                            resetState.setText("This is not an registered email, you can create new account");
                        }
                        else
                            {
                            firebaseAuth.sendPasswordResetEmail(emailAddress.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                progressBar.setVisibility(View.GONE);
                                if(task.isSuccessful())
                                {
                                    resetState.setText("An email to reset has been sent to your email address");
                                }
                                else
                                    {
                                    resetState.setText(task.getException().getMessage());
                                    }
                                }
                            });
                        }
                    }
                });

                break;
            case R.id.returnBtn:
                Intent i = new Intent(ForgettingPassActivity.this, MainActivity.class);
                startActivity(i);
                break;

        }
    }
}