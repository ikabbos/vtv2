package com.example.vt2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import org.w3c.dom.Text;

public class VaccineSubActivity extends AppCompatActivity {

    private static final String[] DISEASES = new String[]{"COVID-19","Adenovirus","Anthrax","Chickenpox","Cholera","Dengue","Diphtheria","Ebola","Flu","Hepatitis A","Hepatitis B","Hib","HPV","Japanese Encephalitis",
            "Malaria","Marburg","Measles","Meningitis","Monkeypox","Mumps","Pneumococcal","Polio","Rabies","Rotavirus","Rubella","Shingles","Smallpox","Tetanus","Tickborne Enchephalitis","Tuberculosis","Typhoid Fever",
            "Whooping Cough","Yellow Fever"};

    private SwitchCompat switch1;
    private LinearLayout layoutOn;
    private LinearLayout layoutOff;
    //private TextView textOn;
    //private TextView textOff;
    private TextView formText;

    Uri filePath;
    Button chooseImg;
    int PICK_IMAGE_REQUEST = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccine_sub);

        switch1 = (SwitchCompat) findViewById(R.id.switch1);
        layoutOn = (LinearLayout) findViewById(R.id.layoutExempt);
        layoutOff = (LinearLayout)findViewById(R.id.layoutVacc);
        //textOn = (TextView) findViewById(R.id.textOn);
        //textOff = (TextView) findViewById(R.id.textOff);
        formText = (TextView) findViewById(R.id.subText);

        chooseImg = (Button)findViewById(R.id.chooseImg);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, DISEASES);
        MaterialAutoCompleteTextView textViewDiseases = (MaterialAutoCompleteTextView) findViewById(R.id.diseases);
        textViewDiseases.setAdapter(adapter);

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    layoutOn.setVisibility(View.VISIBLE);
                    layoutOff.setVisibility(View.GONE);
                    formText.setText(R.string.form_text_ex);
                    //textOn.setVisibility(View.VISIBLE);
                    //textOff.setVisibility(View.GONE);
                }
                else{
                    layoutOn.setVisibility(View.GONE);
                    layoutOff.setVisibility(View.VISIBLE);
                    formText.setText(R.string.form_text_va);
                    //textOn.setVisibility(View.GONE);
                    //textOff.setVisibility(View.VISIBLE);
                }
            }
        });

        chooseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
        }
    }
}